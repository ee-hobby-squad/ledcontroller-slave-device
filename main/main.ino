#define USE_UDP
//#define USE_SERIAL

#ifdef USE_UDP
#include <WiFi.h>
#include <WiFiUdp.h>
#endif

#include <FastLED.h>
#include "config.hpp"

#ifdef USE_UDP
#define UDP_TX_PACKET_MAX_SIZE (3000)
#endif

// Array of leds
CRGB leds[numLeds];

char allowData = 'A';

#ifdef USE_UDP
uint8_t packetBuffer[UDP_TX_PACKET_MAX_SIZE];

WiFiUDP Udp;

void sendData(const char* to, uint16_t port, uint8_t *d, size_t len)
{
    Udp.beginPacket(to, port);
    Udp.write(d, len);
    Udp.endPacket();
}

void setupUDP() {
    WiFi.begin(ssid, pwd);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
    }

    Udp.begin(6600);
}

void loopUDP() {
    uint8_t d = allowData;
    sendData("192.168.2.21", 6603, &d, 1);
    
    int packetSize = Udp.parsePacket();
    if (packetSize == numLeds * 3) {
        int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
        memcpy(leds, packetBuffer, numLeds * 3);
        FastLED.show();
    }
    delay(16);
}
#endif

#ifdef USE_SERIAL
void setupSerial()
{
    Serial.begin(115200);
}

void loopSerial()
{
    Serial.write(allowData);

    if (Serial.available())
    {
        Serial.readBytes((char*)leds, numLeds * 3);
        FastLED.show();
    }
}
#endif

void setup()
{
    FastLED.addLeds<WS2812B, dataPin, GRB>(leds, numLeds);  // GRB ordering is typical
#ifdef USE_UDP
    setupUDP();
#elif defined(USE_SERIAL)
    setupSerial();
#endif
}

void loop()
{
#ifdef USE_UDP
    loopUDP();
#elif defined(USE_SERIAL)
    loopSerial();
#endif
}
